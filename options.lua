return {
  opt = {
    conceallevel = 2, -- enable conceal
    list = true, -- show whitespace characters
    listchars = { tab = "│→", extends = "⟩", precedes = "⟨", trail = "·", nbsp = "␣" },
    showbreak = "↪ ",
    showtabline = (vim.t.bufs and #vim.t.bufs > 1) and 2 or 1,
    spellfile = vim.fn.expand "~/.config/nvim/lua/user/spell/en.utf-8.add",
    splitkeep = "screen",
    swapfile = false,
    thesaurus = vim.fn.expand "~/.config/nvim/lua/user/spell/mthesaur.txt",
    wrap = true, -- soft wrap lines
  },
  g = {
    resession_enabled = true,
    tex_flavor = "latex",
    vimtex_view_method = "sioyek",
    vimtex_callback_progpath = "/Users/skoler/Downloads/nvim-macos/bin/nvim",
    -- vimtex_quickfix_mode = 0,
    -- vimtex_view_skim_sync = 1, -- allow forward search after every successful compilation
    -- vimtex_view_skim_activate = 1, -- change focus to skim after command `:VimtexView` is given
  },
}

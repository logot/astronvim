-- local ih = require "inlay-hints"

return {
  -- on_attach = function(client, bufnr)
  --   if client.server_capabilities.inlayHintProvider then
  --     local inlay_hints_group = vim.api.nvim_create_augroup("InlayHints", { clear = true })
  --
  --     local mode = vim.api.nvim_get_mode().mode
  --     vim.lsp.inlay_hint.enable(bufnr, mode == "n" or mode == "v")
  --
  --     vim.api.nvim_create_autocmd("InsertEnter", {
  --       group = inlay_hints_group,
  --       buffer = bufnr,
  --       callback = function() vim.lsp.inlay_hint(bufnr, false) end,
  --     })
  --     vim.api.nvim_create_autocmd("InsertLeave", {
  --       group = inlay_hints_group,
  --       buffer = bufnr,
  --       callback = function() vim.lsp.inlay_hint(bufnr, true) end,
  --     })
  --   end
  -- end,

  settings = {
    gopls = {
      hints = {
        assignVariableTypes = true,
        compositeLiteralFields = true,
        compositeLiteralTypes = true,
        constantValues = true,
        functionTypeParameters = true,
        parameterNames = true,
        rangeVariableTypes = true,
      },
    },
  },
}

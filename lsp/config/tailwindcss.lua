return {
  filetypes = { "html", "elixir", "eelixir", "heex", "typescriptreact", "javascriptreact", "astro" },
  init_options = {
    userLanguages = {
      elixir = "html-eex",
      eelixir = "html-eex",
      heex = "html-eex",
    },
  },
  settings = {
    tailwindCSS = {
      experimental = {
        classRegex = {
          'class:[:]\\s*"([^"]*)"',
        },
      },
    },
  },
}

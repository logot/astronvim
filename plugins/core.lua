-- local get_icon = require("astronvim.utils").get_icon

return {
  { "max397574/better-escape.nvim", enabled = false },
  { "lukas-reineke/indent-blankline.nvim", enabled = false },
  { "akinsho/toggleterm.nvim", opts = {
    terminal_mappings = false,
  } },
  { "rcarriga/nvim-notify", opts = {
    background_colour = "#000000",
    timeout = 0,
  } },
  {
    "lewis6991/gitsigns.nvim",
    opts = {
      signcolumn = false,
      numhl = true,
      current_line_blame_opts = { ignore_whitespace = true },
    },
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    opts = {
      filesystem = {
        hijack_netrw_behavior = "open_default",
        filtered_items = {
          always_show = { ".github", ".gitignore", ".env" },
        },
      },
    },
  },
  { "christoomey/vim-tmux-navigator", event = "VeryLazy" },
  {
    "andweeb/presence.nvim",
    event = "VeryLazy",
    opts = { main_image = "neovim" },
  },
  {
    "folke/twilight.nvim",
    keys = { { "<leader>uW", "<cmd>Twilight<cr>", desc = "Toggle Twilight" } },
    cmd = {
      "Twilight",
      "TwilightEnable",
      "TwilightDisable",
    },
  },
  {
    "diepm/vim-rest-console",
  },
  "presence-nvim",
}

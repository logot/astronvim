return {
  -- c/c++
  {
    "p00f/clangd_extensions.nvim",
    ft = { "c", "cpp", "objc", "objcpp", "cuda", "proto" },
    opts = {
      inlay_hints = {
        -- inline = vim.fn.has "nvim-0.10" == 1,
        inline = false,
        only_current_line = false,
        only_current_line_autocmd = "CursorHold",
        show_parameter_hints = true,
        parameter_hints_prefix = "<- ",
        other_hints_prefix = "=> ",
        max_len_align = false,
        max_len_align_padding = 1,
        right_align = false,
        right_align_padding = 7,
        highlight = "Comment",
        priority = 100,
      },

      ast = {
        kind_icons = {
          Compound = "",
          Recovery = "",
          TranslationUnit = "",
          PackExpansion = "",
          TemplateTypeParm = "",
          TemplateTemplateParm = "",
          TemplateParamObject = "",
        },
        role_icons = {
          type = "",
          declaration = "",
          expression = "",
          specifier = "",
          statement = "",
          ["template argument"] = "",
        },
        highlights = {
          detail = "Comment",
        },
      },
    },
    config = true,
  },
  -- rust
  {
    "Saecki/crates.nvim",
    event = { "BufRead Cargo.toml" },
    config = true,
  },
  {
    "simrat39/rust-tools.nvim",
    opts = function()
      local ok, mason_registry = pcall(require, "mason-registry")
      local adapter ---@type any
      if ok then
        -- rust tools configuration for debugging support
        local codelldb = mason_registry.get_package "codelldb"
        local extension_path = codelldb:get_install_path() .. "/extension/"
        local codelldb_path = extension_path .. "adapter/codelldb"
        local liblldb_path = vim.fn.has "mac" == 1 and extension_path .. "lldb/lib/liblldb.dylib"
          or extension_path .. "lldb/lib/liblldb.so"
        adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path)
      end
      return {
        dap = {
          adapter = adapter,
        },
        tools = {
          on_initialized = function()
            vim.cmd [[
                augroup RustLSP
                  autocmd CursorHold                      *.rs silent! lua vim.lsp.buf.document_highlight()
                  autocmd CursorMoved,InsertEnter         *.rs silent! lua vim.lsp.buf.clear_references()
                  autocmd BufEnter,CursorHold,InsertLeave *.rs silent! lua vim.lsp.codelens.refresh()
                augroup END
              ]]
          end,
        },
      }
    end,
  },
  -- { "simrat39/inlay-hints.nvim", event = "User AstroFile", config = function() require("inlay-hints").setup() end },
  -- zig
  {
    "ziglang/zig.vim",
    init = function() vim.g.zig_fmt_autosave = 1 end,
  },
  {
    "https://codeberg.org/NTBBloodbath/zig-tools.nvim",
    -- Load zig-tools.nvim only in Zig buffers
    ft = { "zig" },
    opts = {},
    dependencies = {
      "akinsho/toggleterm.nvim",
      "nvim-lua/plenary.nvim",
    },
  },
  -- fish
  { "dag/vim-fish", ft = "fish", event = { "BufRead config.fish" } },
  -- sql
  { "tpope/vim-dadbod", cmd = "DB" },
  { "kristijanhusak/vim-dadbod-ui", cmd = "DB" },
  { "kristijanhusak/vim-dadbod-completion", cmd = "DB" },
  -- tailwind
  {
    "roobert/tailwindcss-colorizer-cmp.nvim",
    -- optionally, override the default options:
    config = function()
      require("tailwindcss-colorizer-cmp").setup {
        color_square_width = 2,
      }
    end,
  },
  -- latex
  -- { "lervag/vimtex" },
  -- gleam
  {
    "gleam-lang/gleam.vim",
    ft = "gleam",
    event = { "BufRead *.gleam" },
    dependencies = {
      {
        "neovim/nvim-lspconfig",
        config = function() require("lspconfig").gleam.setup {} end,
      },
    },
  },
}

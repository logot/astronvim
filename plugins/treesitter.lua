return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    if type(opts.ensure_installed) == "table" then
      vim.list_extend(opts.ensure_installed, {
        -- c/c++
        "c",
        "cpp",
        "cmake",
        -- rust --
        "ron",
        "rust",
        -- zig --
        "zig",
        -- erlang --
        "erlang",
        -- elixir --
        "elixir",
        "heex",
        "eex",
        "heex",
        -- go --
        "go",
        "gomod",
        "gowork",
        "gosum",
        -- sql --
        "sql",
        -- python --
        "ninja",
        "python",
        "rst",
        -- java --
        "java",
        -- web --
        "html",
        "javascript",
        "typescript",
        "tsx",
        "astro",
        -- iac --
        "dockerfile",
        "terraform",
        "hcl",
        -- latex --
        -- "bibtex",
        -- "latex",
        -- org --
        "org",
        "toml",
        "yaml",
        "json",
        "json5",
        "jsonc",
        "xml",
        -- shell --
        "fish",
      })
      opts.autotag = {
        enable = true,
      }
      opts.ignore_install = {
        "latex",
      }
      opts.highlight = {
        enable = true,
        disable = { "latex" },
      }
    end
  end,
}

return {
  "AstroNvim/astrocommunity",
  { import = "astrocommunity.colorscheme.tokyonight-nvim" },
  { import = "astrocommunity.colorscheme.kanagawa-nvim" },

  -- ThePrimeagen
  { import = "astrocommunity.editing-support.refactoring-nvim" },
  -- presentatioon
  { import = "astrocommunity.color.transparent-nvim" },
  -- clipboard store
  { import = "astrocommunity.register.nvim-neoclip-lua" },
  -- LSP
  { import = "astrocommunity.markdown-and-latex.vimtex" },
  -- editing
  { import = "astrocommunity.editing-support.multicursors-nvim" },
}

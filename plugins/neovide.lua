--------------------------------------------------
-- Helper function for transparency formatting
--------------------------------------------------

-- Helper function for transparency formatting
local alpha = function() return string.format("%x", math.floor(255 * vim.g.neovide_transparency_point or 0.8)) end
-- Set transparency and background color (title bar color)
vim.g.neovide_transparency = 0.0
vim.g.neovide_transparency_point = 0.99
-- vim.g.neovide_transparency_point = 0.80
vim.g.neovide_background_color = "#0f1117" .. alpha()
-- Add keybinds to change transparency
local change_transparency = function(delta)
  vim.g.neovide_transparency_point = vim.g.neovide_transparency_point + delta
  vim.g.neovide_background_color = "#0f1117" .. alpha()
end
vim.keymap.set({ "n", "v", "o" }, "<D-]>", function() change_transparency(0.01) end)
vim.keymap.set({ "n", "v", "o" }, "<D-[>", function() change_transparency(-0.01) end)
vim.g.neovide_floating_blur_amount_x = 5.0
vim.g.neovide_floating_blur_amount_y = 5.0
--------------------------------------------------
-- font
--------------------------------------------------
vim.o.guifont = "JetBrainsMono Nerd Font"
-- vim.o.guifont = "Iosevka Nerd Font"
-- vim.o.guifont = "FiraCode Nerd Font"
-- vim.o.guifont = "Terminess Nerd Font:h14"
-- underline
-- vim.g.neovide_underline_automatic_scaling = true

--------------------------------------------------
-- allow copy and paste
--------------------------------------------------
if vim.g.neovide then
  vim.g.neovide_input_use_logo = 1 -- enable use of the logo (cmd) key
  vim.keymap.set("v", "<D-c>", '"+y') -- Copy
  vim.keymap.set("n", "<D-v>", '"+P') -- Paste normal mode
  vim.keymap.set("v", "<D-v>", '"+P') -- Paste visual mode
  vim.keymap.set("c", "<D-v>", "<C-R>+") -- Paste command mode
  vim.keymap.set("i", "<D-v>", '<ESC>l"+Pli') -- Paste insert mode
end

-- Allow clipboard copy paste in neovim
vim.g.neovide_input_use_logo = 1
vim.api.nvim_set_keymap("", "<D-v>", "+p<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("!", "<D-v>", "<C-R>+", { noremap = true, silent = true })
vim.api.nvim_set_keymap("t", "<D-v>", "<C-R>+", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<D-v>", "<C-R>+", { noremap = true, silent = true })

--------------------------------------------------
-- Dynamically change the scale
--------------------------------------------------
vim.g.neovide_scale_factor = 1.0
local change_scale_factor = function(delta) vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta end
vim.keymap.set("n", "<D-=>", function() change_scale_factor(1.25) end)
vim.keymap.set("n", "<D-->", function() change_scale_factor(1 / 1.25) end)

--------------------------------------------------
-- Mouse
--------------------------------------------------
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_cursor_vfx_mode = "ripple"

--------------------------------------------------
-- Quility settings
--------------------------------------------------
vim.g.neovide_confirm_quit = true
vim.g.neovide_remember_window_size = true

return {}
